(function($) {
  $.fn.menumaker = function(options) {
      var cssmenu = $(this), settings = $.extend({
//        title: "Menu",
        format: "dropdown",
        sticky: false
      }, options);

      return this.each(function() {
        cssmenu.prepend('<div id="menu-button"></div>');
        $(this).find("#menu-button").on('click', function(){
          $(this).toggleClass('menu-opened');
          var mainmenu = $(this).next('ul');
          if (mainmenu.hasClass('open')) { 
            mainmenu.hide().removeClass('open');
          }
          else {
            mainmenu.show().addClass('open');
            if (settings.format === "dropdown") {
              mainmenu.find('ul').show();
            }
          }
        });

        cssmenu.find('li ul').parent().addClass('has-sub');

        multiTg = function() {
          cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
          cssmenu.find('.submenu-button').on('click', function() {
            $(this).toggleClass('submenu-opened');
            if ($(this).siblings('ul').hasClass('open')) {
              $(this).siblings('ul').removeClass('open').hide();
            }
            else {
              $(this).siblings('ul').addClass('open').show();
			  $(this).siblings('ul').addClass('animated fadeIn');
				
            }
          });
        };

        if (settings.format === 'multitoggle') multiTg();
        else cssmenu.addClass('dropdown');

        if (settings.sticky === true) cssmenu.css('position', 'fixed');

        resizeFix = function() {
          if ($( window ).width() > 992) {
            cssmenu.find('ul').show();
          }

          if ($(window).width() <= 992) {
            cssmenu.find('ul').hide().removeClass('open');
          }
        };
        resizeFix();
        return $(window).on('resize', resizeFix);

      });
  };
})(jQuery);

$(document).ready(function(){
			$("#menu").menumaker({
		//   title: "Menu",
		   format: "multitoggle"
		});
	$('.carousel').carousel({
	  autoplay: false
	})
});




    $(window).scroll(function(){
            var wp = $(window).scrollTop();
                    var scrollPoint = 130;
                    if(wp >= scrollPoint){
                                $("header").addClass("fixed");
                    }else{
                                $("header").removeClass("fixed");
                    }

        });
